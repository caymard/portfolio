FROM node:18-slim as build
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . /app
RUN npm run build

FROM nginx:1.23.3-alpine as production
COPY --from=build /app/dist/apps/portfolio /usr/share/nginx/html/
