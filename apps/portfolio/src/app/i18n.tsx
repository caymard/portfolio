import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import I18nextBrowserLanguageDetector from "i18next-browser-languagedetector";
import en from "./translations/en.json";
import fr from "./translations/fr.json";

const resources = {
  fr,
  en,
};

export const availableLanguages = Object.keys(resources);

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .use(I18nextBrowserLanguageDetector)
  .init({
    resources,
    ns: ["common", "profile"],
    defaultNS: "common",
    fallbackLng: "fr",
  });

export default i18n;
