// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { useTranslation, Trans } from "react-i18next";
import { LanguageSelector } from "./components/LanguageSelector";

export function App() {
  const { t, i18n } = useTranslation();
  return (
    <div className="container">
      <header>
        <h1>Clément AYMARD</h1>
        <h2>Site Reliability Engineer</h2>
      </header>
      <main>
        <p>{t("profile:speciality")}</p>
        <p>
          <Trans
            i18nKey="profile:current_job"
            components={{
              italic: <em />,
              jobLink: <a href="https://api.video" />,
            }}
          ></Trans>
        </p>
        <p>
          <Trans
            i18nKey="profile:cadevops"
          ></Trans>
        </p>
        <p>
          <Trans
            i18nKey="profile:formation"
            components={{ n7Link: <a href="https://www.enseeiht.fr" /> }}
          ></Trans>
        </p>
        <p>
          <a href="https://www.linkedin.com/in/claymard">Linkedin</a>
          {" - "}
          <a href="https://github.com/caymard">Github</a>
          {" - "}
          <a href="https://gitlab.com/caymard">Gitlab</a>
          {" - "}
          <Trans
            i18nKey="profile:resume"
            components={{
              resumeLink:
                i18n.language === "fr" ? (
                  <a href="https://caymard-portfolio.s3.fr-par.scw.cloud/CV.pdf" />
                ) : (
                  <a href="https://caymard-portfolio.s3.fr-par.scw.cloud/Resume.pdf" />
                ),
            }}
          ></Trans>
        </p>
      </main>
      <footer>
        <LanguageSelector />
        <br />
        <p className="stack">
          <Trans
            i18nKey="footer:run_on_docker"
            components={{
              dockerLink: <a href="https://docker.com" />,
              reactLink: <a href="https://react.com" />,
            }}
          ></Trans>
        </p>
      </footer>
    </div>
  );
}

export default App;
