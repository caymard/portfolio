import { useTranslation } from "react-i18next";

import "../i18n";
import { availableLanguages } from "../i18n";

export function LanguageSelector() {
  const { i18n } = useTranslation();
  return (
    <div className="language">
      <select
        defaultValue={i18n.language}
        onChange={(e) => i18n.changeLanguage(e.target.value)}
      >
        {availableLanguages.map((language) => (
          <option key={language}>{language}</option>
        ))}
      </select>
    </div>
  );
}
